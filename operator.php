<?php

include_once "vendor/autoload.php";
use Pondit\Operator\Addition;
use Pondit\Operator\Subtraction;
use Pondit\Operator\Multiplication;
use Pondit\Operator\Division;
use Pondit\Operator\Modulus;
use Pondit\Operator\Exponentiation;
use Pondit\Operator\Optional;
use Pondit\Format\Formatting;


$sum = new Addition(001245);
$format=new Formatting();
$format->h2($sum->slNumber);
$addition = $sum->addition(12,12);
$format->pre($addition);
echo "<hr/>";

$sub = new Subtraction(1542);
$subtraction=$sub->subtraction(1500,1000);
$format->h2($sub->roll);
$format->pre($subtraction);
echo "<hr/>";

$multiplication = new 	Multiplication("Nighat Parvin");
$result =$multiplication->multiplication(12,12);
$format->h2($multiplication->name);
$format->pre($result);
echo "<hr/>";


$division=  new Division(12);
$result=$division->division(45,5);
$format->h2($division->reg);
$format->pre($result);
echo "<hr/>";

$modulus =new Modulus(1245);
$result=$modulus->modulus(15,4);
$format->h2($modulus->mlNumber);
$format->pre($result);
echo "<hr/>";



$exponentiation = new Exponentiation("Expon is Ex");
$result= $exponentiation->exponentiation(5,2);
$format->h2($exponentiation->expon);
$format->pre($result);
echo "<hr/>";
?>




<form action="" METHOD="post">
    <table>
        <tr>
            <td>Enter The First Number</td>
            <td><input type="number" name="number1"/></td>
        </tr>
        <tr>
            <td>Enter The Second Number</td>
            <td><input type="number" name="number2"/></td>
        </tr>
        <tr>
            <td></td>
            <td><input type="submit" name="calculation" value="Calculate"/></td>
        </tr>
    </table>
</form>

<?php

if(isset($_POST["calculation"])){
    $number1 =$_POST["number1"];
    $number2 =$_POST["number2"];


    if(empty($number1)or empty($number2)){

        echo "<span style='color: darkred'> Field must not be empty.</span><br/>";
    } else{
        echo "<b>First Number is:</b>".$number1. "<br/> <b> Second Number is:</b>". $number2."<br/>";

        $optional = new Optional();
        echo $optional->addition($number1,$number2)."<br/>";
        echo $optional->subtraction($number1,$number2)."<br/>";
        echo $optional->modulus($number1,$number2)."<br/>";
        echo $optional->division($number1,$number2)."<br/>";
       echo $optional->exponentiation($number1,$number2);


    }

}



