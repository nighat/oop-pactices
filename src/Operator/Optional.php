<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 3/18/2018
 * Time: 4:42 PM
 */

namespace Pondit\Operator;


class Optional
{
    public function addition($number1,$number2){


        $result = $number1+$number2;
        return "Addition Number:-".$result;
    }

    public function subtraction($number1,$number2){

        $result = $number1-$number2;
        return "Subtraction Number:-".$result;
    }

    public function division($number1,$number2){

        $result = $number1/$number2;
        return "My Division is ".$result;
    }

    public function modulus($number1,$number2){

        $result = $number1%$number2;
        return "My Modulus Number is ".$result;
    }

    public function exponentiation($number1,$number2){

        $result = $number1**$number2;
        return $result;
    }
}