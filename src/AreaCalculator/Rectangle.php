<?php

namespace Pondit\AreaCalculator;

class Rectangle
{
  public $width;
  public $length;

  public function __construct($width,$length)
  {
      $this->width=$width;
      $this->length=$length;
  }

    public function getArea(){

      $area=$this->width*$this->length;
      return $area."<br/>";
  }
}