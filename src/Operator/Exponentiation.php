<?php


namespace Pondit\Operator;


class Exponentiation
{
   public $expon = null;
   public function __construct($expon)
   {
       $this->expon = "Exponentiation ".$expon;
   }
   public function exponentiation($number1,$number2){

       $result = $number1**$number2;
       return $result;
   }
}