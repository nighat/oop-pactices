<?php

namespace Pondit\Operator;


class Division
{
  public $reg=null;
  public function __construct($reg)
  {
      $this->reg="My Reg Number:-".$reg;
  }
  public function division($number1,$number2){

      $result = $number1/$number2;
      return "My Division is ".$result;
  }
}