<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 3/18/2018
 * Time: 3:25 PM
 */

namespace Pondit\Format;


class Formatting
{


    public function pre($detail){

        echo "<pre style='color: blue'>";
        echo $detail;
        echo "</pre>";
    }
    public function h2($detail){
        echo "<h2 style='color: green'>";
        echo $detail;
        echo "</h2>";
    }
}